<?php
  include('connection/config.php');
  include("lock.php");

  $myindex = $_GET['id'];

  $userAccountId = '';
  $login_user = $_SESSION['login_user'];
  $sql = "SELECT accountid FROM users WHERE email='$login_user'";
  $result = mysqli_query($con, $sql);
  $count = mysqli_num_rows($result);

  $error = '';
  // If result matched $myemail and $mypassword, table row must be 1 row
  if($count==1)
  {
    $row = mysqli_fetch_assoc($result);
    if ( isset($row) ) {
      $userAccountId = $row['accountid'];
    }
  }

?>
<!doctype html>
<html>
<head>
  <title>News</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">  <!-- Latest compiled and minified CSS -->
  <!-- Bootstrap -->
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <link href="static/css/style.css" rel="stylesheet">
</head>
<body>
  <div class="container">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Seguretat en Bases de Dades</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Latch <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <?php if (isset($userAccountId)) { ?>
                  <li><a href="unpair.php">Unpair</a></li>
                <?php } else { ?>
                  <li><a href="pairing.php">Pair</a></li>
                <?php } ?>
                <li><a href="latch_status.php">Status</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="news.php">Llista de notícies</a></li>
            <li><a href="logout.php">No ets <?php echo $_SESSION['login_user'] ?>?</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <?php if (isset($myindex)) {
      try {
        $sql = "select title, body from news where id=$myindex";
        $result = mysqli_query($con, $sql);
        $row = mysqli_fetch_assoc($result);
        if ( isset($row) ) {
          echo '<h2>'.$row['title'].'</h2>';
          echo $row['body'];
        }
      } catch ( Exception $e ) {
        echo 'Error: '.$e->getMessage().'<br>';
      }
    } else {
      echo '<h2>Llistat de not&iacute;cies</h2>';
      try {
        $sql = "select id, title from news order by datetime desc";
        $result = mysqli_query($con, $sql);
        $count = mysqli_num_rows($result);
        if ($count == 0) {
          echo '<h3>No hi ha notícies per veure</h3>';
        } else {
          echo '<ul>';
          while($row = mysqli_fetch_assoc($result)) {
            echo '<li>';
            echo '<a href="news.php?id='.$row['id'].'">'.$row['title'].'</a>';
            echo '</li>';
          }
          echo '</ul>';
        }
      } catch ( Exception $e ) {
        echo 'Error: '.$e->getMessage().'<br>';
      }

    } ?>
  </div>
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
