<?php
  require_once('connection/config.php');
  include("lock.php");
  require_once("latch/Latch.php");
  require_once("latch/LatchResponse.php");
  require_once("latch/Error.php");

  $userAccountId = '';
  $login_user = $_SESSION['login_user'];
  $sql = "SELECT accountid FROM users WHERE email='$login_user'";
  $result = mysqli_query($con, $sql);
  $count = mysqli_num_rows($result);

  $error = '';
  $info = '';
  // If result matched $myemail and $mypassword, table row must be 1 row
  if($count==1)
  {
    $row = mysqli_fetch_assoc($result);
    if ( isset($row) ) {
      $userAccountId = $row['accountid'];
    }
  }

  if($_SERVER["REQUEST_METHOD"] == "POST") {
    $mypairing = $_POST['pairing'];

    try {
      $api = new Latch($LATCH_APP_ID, $LATCH_APP_SECRET);
      $pairResponse = $api->pair($mypairing);
      $data = $pairResponse->getData();
      if ( !is_null($data) && property_exists($data, "accountId") ) {
        $accountId = $data->accountId;
        $sql = "update users set accountid='$accountId' where email = '$login_user'";
        if ( mysqli_query($con, $sql) ) {
          $info = "Usuari emparellat correctament.";
          $userAccountId = $accountId;
        }
        else {
          $error = mysqli_error($con);
        }
      } else {
        $error = "No s'ha pogut crear l'emparellament";
      }
    } catch ( Exception $e ) {
      echo 'Error: '.$e->getMessage().'<br>';
    }
  }
?>
<!doctype html>
<html>
<head>
  <title>Pairing with Latch</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">  <!-- Latest compiled and minified CSS -->
  <!-- Bootstrap -->
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <link href="static/css/style.css" rel="stylesheet">
</head>
<body>
  <div class="container">
    <?php if ($error) { ?>
      <br>
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Error:</strong> <?php echo $error; ?>
      </div>
    <?php } ?>
    <?php if ($info) { ?>
      <br>
      <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Info:</strong> <?php echo $info; ?>
      </div>
    <?php } ?>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Seguretat en Bases de Dades</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Latch <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <?php if (isset($userAccountId)) { ?>
                  <li><a href="unpair.php">Unpair</a></li>
                <?php } else { ?>
                  <li class="active"><a href="pairing.php">Pair</a></li>
                <?php } ?>
                <li><a href="latch_status.php">Status</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="news.php">Llista de notícies</a></li>
            <li><a href="logout.php">No ets <?php echo $_SESSION['login_user'] ?>?</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <?php if (isset($accountId)) { ?>
      <h3>Emparellat amb Latch correctament!</h3>
    <?php } else { ?>
      <form class="form-signin" role="form" method="post" action="">
        <h2>Emparella't amb Latch</h2>
        <label for="inputLatch" class="sr-only">Codi d'emparellament</label>
        <input type="text" id="inputLatch" name="pairing" class="form-control" placeholder="Codi d'emparellament" required="true" autofocus="">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Emparella't!</button>
      </form>
    <?php } ?>
  </div>
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
