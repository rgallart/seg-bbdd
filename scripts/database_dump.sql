-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Dec 23, 2014 at 02:03 PM
-- Server version: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ssbbdd`
--

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
`id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `body` text NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Taula de notícies';

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `body`, `datetime`) VALUES
(1, 'Title 1', 'Body 1', '2014-12-23 07:52:35'),
(2, 'Title 2', 'Body 2', '2014-12-23 07:52:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `email` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `accountid` varchar(100) DEFAULT NULL COMMENT 'Latch Account Id'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Taula d''usuaris';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`email`, `name`, `password`, `accountid`) VALUES
('guest@example.com', 'Guest', 'guest', NULL),
('rgallart@uoc.edu', 'Ramon Maria Gallart (UOC)', '12345', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`email`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
