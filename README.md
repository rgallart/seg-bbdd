# Seguretat en Bases de Dades

Aquesta és la pràctica per al primer semestre del curs 2014/15 de l'assignatura **Seguretat en bases de dades** del MISTIC de la UOC.

L'aplicació està feta en PHP 5.6.2 i utilitza MySQL 5.0.11.

Bàsicament això és així ja que és la configuració que ve amb MAMP (http://www.mamp.info/en/) a data d'avui (23/12/2014).

Conté un directori scripts que conté un dump de la base de dades de mostra. Podeu usar aquesta mostra amb la configuració que hi ha en el fitxer connection/config.php a l'hora de crear la base de dades en el vostre sistema.

L'aplicació té vàries fonts d'injecció SQL. Una d'elles és al paràmetre *id* de la pàgina news.php. Amb SQLMap (http://sqlmap.org/) és fàcil obtenir les credencials dels usuaris de la base de dades.

La idea és que si s'integra el servei amb Latch, encara que ens robin les credencials, podem negar l'accés al servei i evitar que ens robin la identitat.

Salutacions i Bons Nadals!
