<?php
  include('connection/config.php');
  require_once("latch/Latch.php");
  require_once("latch/LatchResponse.php");
  require_once("latch/Error.php");

  session_start();
  if($_SERVER["REQUEST_METHOD"] == "POST")
  {
    // username and password sent from Form
    $myemail = $_POST['email'];
    $mypassword = $_POST['password'];

    $sql = "SELECT accountid FROM users WHERE email='$myemail' and password='$mypassword'";
    $result = mysqli_query($con, $sql);
    $count = mysqli_num_rows($result);

    $error = '';
    $userAccountId = '';
    // If result matched $myemail and $mypassword, table row must be 1 row
    if($count==1)
    {
      // Comprovem Latch
      $row = mysqli_fetch_assoc($result);
      if ( isset($row) ) {
        $userAccountId = $row['accountid'];
      }

      $canAccess = FALSE;
      if (isset($userAccountId) && !empty($userAccountId)) {
        $api = new Latch($LATCH_APP_ID, $LATCH_APP_SECRET);
        $status = $api->status($userAccountId);
        if ( !empty($status) && $status->getData() != null ) {
          $operations = $status->getData()->operations;
          $operation = $operations->$LATCH_APP_ID;
          $canAccess = $operation->status == 'on';
        }
      } else {
        $canAccess = TRUE;
      }


      if ($canAccess) {
        $_SESSION['login_user'] = $myemail;
        header("Location: news.php");
      } else {
        $error = "L'usuari $myemail no pot accedir a l'aplicació";
      }
    }
    else
    {
      $error="Credencials incorrectes";
    }
  }
 ?>
<!doctype html>
<html>
<head>
  <title>Login</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">  <!-- Latest compiled and minified CSS -->
  <!-- Bootstrap -->
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <link href="static/css/style.css" rel="stylesheet">
</head>
<body>
  <div class="container">
    <?php if ($error) { ?>
      <br>
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Error:</strong> <?php echo $error; ?>
      </div>
    <?php } ?>
    <form class="form-signin" role="form" method="post" action="">
      <h2>Identifica't per llegir les notícies</h2>
      <label for="inputEmail" class="sr-only">Correu electrònic</label>
      <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Correu electrònic" required="" autofocus="">
      <label for="inputPassword" class="sr-only">Contrasenya</label>
      <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Contrasenya" required="">
      <button class="btn btn-lg btn-primary btn-block" type="submit">Entra</button>
    </form>
  </div>
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
